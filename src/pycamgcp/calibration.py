"""Main module."""

import csv
import os
from pprint import pprint

import cameratransform as ct
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import numpy as np
from pykml import parser
from pyproj import Transformer


def parse_kml(kml_file):
    placemarks = {}
    with open(kml_file) as file:
        root = parser.parse(file).getroot()

    for placemark in root.Document.Folder.Placemark:
        coord_text = placemark.Point.coordinates
        lon, lat, *_ = coord_text.text.strip().split(",")
        coords = (float(lat), float(lon))
        placemarks[placemark.name] = coords

    return placemarks


def parse_csv(csv_file):
    pixel_coords = {}
    with open(csv_file, newline="") as file:
        reader = csv.reader(file)
        for row in reader:
            name = row[0]
            x = int(row[1])
            y = int(row[2])
            coords = (x, y)
            pixel_coords[name] = coords

    return pixel_coords


def transform_coords(coords, crs_from, crs_to):
    if crs_from != crs_to:
        transformer = Transformer.from_crs(crs_from, crs_to)
        return transformer.transform(coords[0], coords[1])
    else:
        return coords


class GCPCalibrator:

    def __init__(
        self,
        sensor_size,
        image_size=None,
        image_file=None,
        focallength_mm=None,
        focallength_x_px=None,
        focallength_y_px=None,
        elevation_m=None,
        tilt_deg=None,
        map_coords=None,
        pixel_coords=None,
        camera_map_coords=None,
        camera_map_coords_crs="EPSG:4326",
        kml_coords_file=None,
        kml_coords_file_crs="EPSG:4326",
        pixel_coords_file=None,
        qgis_coords_file=None,
        qgis_coords_file_crs="EPSG:3857",
        uncertainties_m=None,
        verbose=False,
        internal_crs="EPSG:4326",
    ):  # "EPSG:4326" (lat-lon) or "EPSG:3857" (projected, units=meters)

        self.focallength_mm = focallength_mm
        self.focallength_x_px = focallength_x_px
        self.focallength_y_px = focallength_y_px
        self.sensor_size = sensor_size
        self.image_size = image_size
        self.image_file = image_file
        self.elevation_m = elevation_m
        self.tilt_deg = tilt_deg
        self.map_coords = map_coords
        self.pixel_coords = pixel_coords
        self.camera_map_coords = camera_map_coords
        self.camera_map_coords_crs = camera_map_coords_crs
        self.kml_coords_file = kml_coords_file
        self.kml_coords_file_crs = kml_coords_file_crs
        self.pixel_coords_file = pixel_coords_file
        self.qgis_coords_file = qgis_coords_file
        self.qgis_coords_file_crs = qgis_coords_file_crs
        self.uncertainties_m = uncertainties_m
        self.verbose = verbose
        self.internal_crs = internal_crs

    def _to_internal_crs(self, coords, crs_from):
        if crs_from != self.internal_crs:
            return transform_coords(coords, crs_from, self.internal_crs)
        else:
            return coords

    def parse_gcp_kmlcsv(self, kml_file, kml_file_crs, csv_file):

        # Read map coordinates of landmarks and convert to internal CRS
        self.map_coords = parse_kml(kml_file)
        for v in map_coords.values():
            v = self._to_internal_crs(v, kml_file_crs)

        if self.verbose:
            print(f"Map coordinates read from {kml_file}:")
            pprint(self.map_coords)

        # Take out pole coordinates
        self.camera_map_coords = self.map_coords["Camera"]
        del self.map_coords["Camera"]

        # Read pixel coordinates
        # Origin (0,0) = top left corner
        self.pixel_coords = parse_csv(csv_file)
        print(f"Pixel coordinates read from {csv_file}:")
        if self.verbose:
            pprint(self.pixel_coords)

        return self.map_coords, self.pixel_coords, self.camera_map_coords

    def parse_gcp_qgis(self, csv_file):

        self.map_coords = {}
        self.pixel_coords = {}

        with open(csv_file, newline="") as file:
            reader = csv.reader(file)
            for index, row in enumerate(reader):
                if index < 2:
                    continue  # skip first two lines
                mapx = float(row[0])
                mapy = float(row[1])
                sourcex = float(row[2])
                sourcey = -float(row[3])  # For some reason, Georeferencer uses negative y
                enable = int(row[4])

                if enable == 0:
                    continue

                self.map_coords[str(index)] = self._to_internal_crs((mapx, mapy), self.qgis_coords_file_crs)
                self.pixel_coords[str(index)] = (sourcex, sourcey)

        if self.verbose:
            print(f"Ground control points read from {csv_file}:")
            for key in self.map_coords:
                print(f"{key}: {self.map_coords[key]} -> {self.pixel_coords[key]}")

        return self.map_coords, self.pixel_coords

    def _save_outputs(self, cam, outputfolder, outputfilename):

        # Make output folder
        os.makedirs(outputfolder, exist_ok=True)

        # Save params
        cam.save(os.path.join(outputfolder, outputfilename + ".json"))

        # Save trace plot
        cam.plotTrace()
        plt.tight_layout()
        plt.savefig(os.path.join(outputfolder, outputfilename + "_output_trace.png"), bbox_inches="tight", pad_inches=0)
        plt.close()

        # Save fit plot
        if self.image_file is not None:
            img = mpimg.imread(self.image_file)
            cam.plotFitInformation(img)
            plt.legend()
            plt.axis("off")  # Optional: to turn off axis numbers
            plt.savefig(os.path.join(outputfolder, outputfilename + "_output_image.png"), bbox_inches="tight", pad_inches=0)
            plt.close()

    def calibrate(self, fitparams, iterations=1e4, outputfolder=".", outputfilename="camera"):

        # Check that we have what we need
        if self.map_coords is None or self.pixel_coords is None or self.camera_map_coords is None:

            # Read data files
            if self.kml_coords_file is not None and self.pixel_coords_file is not None:
                self.parse_gcp_kmlcsv(self.kml_coords_file, self.pixel_coords_file)
            elif self.qgis_coords_file is not None:
                self.parse_gcp_qgis(self.qgis_coords_file)
            else:
                raise ValueError("No GCP data provided")

        # Convert camera map coordinates to internal CRS
        self.camera_map_coords = self._to_internal_crs(self.camera_map_coords, self.camera_map_coords_crs)

        # Try to read image_size if not available
        if self.image_size is None:
            if self.image_file is not None:
                img = mpimg.imread(self.image_file)
                self.image_size = (img.shape[1], img.shape[0])
            else:
                raise ValueError("No image size provided")

        # Initialize the camera
        cam = ct.Camera(
            ct.RectilinearProjection(
                focallength_mm=self.focallength_mm,
                focallength_x_px=self.focallength_x_px,
                focallength_y_px=self.focallength_y_px,
                sensor=self.sensor_size,
                image=self.image_size,
            ),
            ct.SpatialOrientation(elevation_m=self.elevation_m, tilt_deg=self.tilt_deg),
            lens=ct.BrownLensDistortion(k1=0, k2=0, k3=0),
        )

        if self.internal_crs == "EPSG:4326":
            cam.setGPSpos(lat=self.camera_map_coords[0], lon=self.camera_map_coords[1])
        else:
            cam.pos_x_m = self.camera_map_coords[0]
            cam.pos_y_m = self.camera_map_coords[1]

        # Iterate keys over the same dictionary (map_coords)
        lm_points_px = np.array([self.pixel_coords[key] for key in self.map_coords])
        lm_points_map = np.array([[self.map_coords[key][0], self.map_coords[key][1], 0] for key in self.map_coords])

        # Convert GPS coordinates to space coordinates
        if self.internal_crs == "EPSG:4326":
            # Must be lat lon
            lm_points_map = lm_points_map[:, [1, 0, 2]]
            lm_points_space = cam.spaceFromGPS(lm_points_map)
        else:
            lm_points_space = lm_points_map

        # Add data to fit
        # cam.addLandmarkInformation(lm_points_px, lm_points_space, [0.5, 0.5, 0.5])
        # cam.addLandmarkInformation(lm_points_px, lm_points_space, [3, 3, 5])
        cam.addLandmarkInformation(lm_points_px, lm_points_space, self.uncertainties_m)

        # Prepare for parameters
        fitparameters = [ct.FitParameter(p, lower=l, upper=u, value=v) for p, l, u, v in fitparams]

        # Run calibration loops
        iterations = [iterations] if not hasattr(iterations, "__iter__") else iterations
        for itindex, itval in enumerate(iterations):

            print("Fitting...")
            trace = cam.metropolis(fitparameters, iterations=itval)
            self._save_outputs(cam, outputfolder, outputfilename + f"_trace{itindex}")

        # Save final results
        self._save_outputs(cam, outputfolder, outputfilename)
