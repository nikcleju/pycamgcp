pycamgcp
========

.. testsetup::

    from pycamgcp import *

.. automodule:: pycamgcp
    :members:
    :undoc-members:
    :special-members: __init__, __len__
