========
Overview
========

.. start-badges

.. list-table::
    :stub-columns: 1

    * - docs
      - |docs|
    * - tests
      - |github-actions| |codecov|
    * - package
      - |version| |wheel| |supported-versions| |supported-implementations| |commits-since|
.. |docs| image:: https://readthedocs.org/projects/pycamgcp/badge/?style=flat
    :target: https://readthedocs.org/projects/pycamgcp/
    :alt: Documentation Status

.. |github-actions| image:: https://github.com/nikcleju/pycamgcp/actions/workflows/github-actions.yml/badge.svg
    :alt: GitHub Actions Build Status
    :target: https://github.com/nikcleju/pycamgcp/actions

.. |codecov| image:: https://codecov.io/gh/nikcleju/pycamgcp/branch/master/graphs/badge.svg?branch=master
    :alt: Coverage Status
    :target: https://app.codecov.io/github/nikcleju/pycamgcp

.. |version| image:: https://img.shields.io/pypi/v/pycamgcp.svg
    :alt: PyPI Package latest release
    :target: https://pypi.org/project/pycamgcp

.. |wheel| image:: https://img.shields.io/pypi/wheel/pycamgcp.svg
    :alt: PyPI Wheel
    :target: https://pypi.org/project/pycamgcp

.. |supported-versions| image:: https://img.shields.io/pypi/pyversions/pycamgcp.svg
    :alt: Supported versions
    :target: https://pypi.org/project/pycamgcp

.. |supported-implementations| image:: https://img.shields.io/pypi/implementation/pycamgcp.svg
    :alt: Supported implementations
    :target: https://pypi.org/project/pycamgcp

.. |commits-since| image:: https://img.shields.io/github/commits-since/nikcleju/pycamgcp/v0.0.0.svg
    :alt: Commits since latest release
    :target: https://github.com/nikcleju/pycamgcp/compare/v0.0.0...master



.. end-badges

Calibrate cameras with Ground Control Points from an image

* Free software: BSD 2-Clause License

Installation
============

::

    pip install pycamgcp

You can also install the in-development version with::

    pip install https://github.com/nikcleju/pycamgcp/archive/master.zip


Documentation
=============


https://pycamgcp.readthedocs.io/


Development
===========

To run all the tests run::

    tox

Note, to combine the coverage data from all the tox environments run:

.. list-table::
    :widths: 10 90
    :stub-columns: 1

    - - Windows
      - ::

            set PYTEST_ADDOPTS=--cov-append
            tox

    - - Other
      - ::

            PYTEST_ADDOPTS=--cov-append tox
